import json

class Day(object):
	def __init__(self, date):
		self.date = date
		self.events = []

class Hunger(object):
	def __init__(self, hungry, id, timestamp):
		self.hungry = hungry
		self.id = id
		self.timestamp = timestamp
		self.description = "<font color=\"red\">hunger:</font> I was hungry"

class Exercise(object):
	def __init__(self, id, timestamp, workout):
		self.id = id
		self.timestamp = timestamp
		self.workout = workout
		self.description = "<font color=\"blue\">workout: </font>" + workout

class Food(object):
	def __init__(self, id, items, timestamp):
		self.type = ""
		self.id = id
		self.items = items
		self.timestamp = timestamp
		self.description = items

	def set_description(self):
		self.description = "<font color=\"green\">" + self.type + ": </font>" + self.description

def process_timestamp(timestamp):
	list = timestamp.split("-")
	date = list[0] + "/" + list[1] + "/" + list[2]
	time = list[3] + ":" + list[4] + ":" + list[5]
	return date + " at " + time


def init_food_from_json(js):
	if "items" in js:
		return Food(js["id"], js["items"], js["timestamp"])

def init_hunger_from_json(js):
	if "hungry" in js:
		return Hunger(js["hungry"], js["id"], js["timestamp"])

def init_ex_from_json(js):
	if "workout" in js:
		return Exercise(js["id"], js["timestamp"], js["workout"])

message = """
<html>
<head></head>
<body>
"""

tail = """
</body>
</html>
"""

message += "<ul>\n"
filename = "healthtracker.json"
with open(filename) as json_data:
	data = json.load(json_data)
all_days = []
for day in data:
	day_obj = Day(day)
	for key in data[day]:
		if key == "hunger":
			for hung in data[day][key]:
				cool = init_hunger_from_json(data[day][key][hung])
				day_obj.events.append(cool)
		if key == "exercise":
			for ex in data[day][key]:
				cool = init_ex_from_json(data[day][key][ex])
				day_obj.events.append(cool)
		if key == "food":
			for type in data[day][key]:
				for instance in data[day][key][type]:
					cool = init_food_from_json(data[day][key][type][instance])
					cool.type = type[:-1]
					cool.set_description()
					day_obj.events.append(cool)
	day_obj.events.sort(key=lambda x: x.timestamp, reverse=False)
        all_days.append(day_obj)

all_days.sort(key=lambda x: x.date, reverse=False)
for day in all_days:
    message += "<div class=\"date\"><h1>" + day.date + "</h1>\n"
    message += "<ul>\n"
    for event in day.events:
        message += "<li><b>" + process_timestamp(event.timestamp) + "</b>: " + event.description + "</li>\n"
        message += "</ul>\n"
        message += "</div>"

message += tail

f = open("demohtml.html", "w")
f.write(message)
